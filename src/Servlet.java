import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

@WebServlet(name = "Servlet", urlPatterns={"/Servlet"})
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            PrintWriter out = response.getWriter();
            response.setContentType("text/html");
            String ipaddress = "";
            if (request.getParameterMap().containsKey("ipaddress")) {
                ipaddress = request.getParameter("ipaddress");
            }
            out.println("<h2>Result</h2>");
            out.println("<pre>" + getWhoIs(ipaddress) + "</pre>");
        } catch (Exception e) {
            System.out.println("Something went wrong.");
            System.out.println(e);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("Not available at this time.");
    }
    public static String getWhoIs(String ipaddress) {
        String result = "";
        try {
            URL arin = new URL("http://whois.arin.net/rest/ip/" + ipaddress);
            HttpURLConnection http = (HttpURLConnection) arin.openConnection();
            http.setRequestProperty("Accept", "application/json");
            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder sBuilder = new StringBuilder();

            String line = null;

            while ((line = reader.readLine()) != null) {
                sBuilder.append(line + "\n");
            }
            String raw = sBuilder.toString();
            result = formatRaw(raw);
        } catch (Exception e) {
            System.out.println("Something went wrong connecting to WhoIs. Try again later.");
            System.err.println(e);
        }
        return result;
    }
    public static String formatRaw(String raw) {
//        String splitter = "\"$\":\"";
        String[] temp = raw.split("@name\":\"");
        String name = temp[1].split("\"")[0];
        String[] temp1 = raw.split("cidrLength");
        String cidrLength = temp1[1].split("\":\"")[1].split("\"")[0];
        String[] temp2 = raw.split("endAddress");
        String endAddress = temp2[1].split("\":\"")[1].split("\"")[0];
        String[] temp3 = raw.split("startAddress");
        String startAddress = temp3[1].split("\":\"")[1].split("\"")[0];
        String result = "Name: " + name + "\n" + "Net Block: " + startAddress + "/" + cidrLength + "\n" + "IP Ranges: " + startAddress + "-" + endAddress + "\n";
        return result;
    }
}
