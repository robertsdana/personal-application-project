import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.*;

class ServletTest {
    @Test
    void getWhoIs() {
        String ip = "8.8.8.8";
        String result = Servlet.getWhoIs(ip);
        assertNotNull(result);
        assertTrue(result.contains("Google"));
        assertTrue(result.contains("8.8.8.0"));
        assertTrue(result.contains("8.8.8.255"));
    }

    @Test
    void formatRaw() throws IOException {
        String ip = "8.8.8.8";
        String raw = "";
        try {
            URL arin = new URL("http://whois.arin.net/rest/ip/" + ip);
            HttpURLConnection http = (HttpURLConnection) arin.openConnection();
            http.setRequestProperty("Accept", "application/json");
            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder sBuilder = new StringBuilder();

            String line = null;

            while ((line = reader.readLine()) != null) {
                sBuilder.append(line + "\n");
            }
            raw = sBuilder.toString();
        } catch (Exception e) {
            System.out.println("Something went wrong with the tests");
            System.err.println(e);
        }
        String result = Servlet.formatRaw(raw);
        assertNotNull(result);
        assertTrue(result.contains("Name: Google"));
        assertTrue(result.contains("Net Block: 8.8.8.0"));
        assertTrue(result.contains("IP Ranges: 8.8.8.0"));
    }
}